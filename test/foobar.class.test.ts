import assert from "assert";
import { Bar } from "../src/classes/Bar.class";
import { Bot } from "../src/classes/Bot.class";
import { Foo } from "../src/classes/Foo.class";
import { Foobar } from "../src/classes/Foobar.class";

describe('[Foobar]', function() {
    const bot = new Bot('Foobar tester');
    it('should return a Foo', function() {
        const foo = new Foo(bot);
        const bar = new Bar(bot);
        const foobar = new Foobar(foo, bar, bot);

        const expected = true;
        const evaluated = foobar.foo instanceof Foo;
        assert.strictEqual(expected, evaluated);
    });
    it('should return a Bar', function() {
        const foo = new Foo(bot);
        const bar = new Bar(bot);
        const foobar = new Foobar(foo, bar, bot);

        const expected = true;
        const evaluated = foobar.bar instanceof Bar;
        assert.strictEqual(expected, evaluated);
    });
});