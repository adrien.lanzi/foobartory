import assert from 'assert';
import { Warehouse } from "../src/classes/warehouse.namespace";
import { Foo } from "../src/classes/Foo.class";
import { Bar } from "../src/classes/Bar.class";
import { Bot } from '../src/classes/Bot.class';
import {Foobar} from "../src/classes/Foobar.class";



describe('[Warehouse]', function() {
    const bot = new Bot('Warehouse tester');
    it('should be empty', function() {
        const expected = 0;
        const evaluated = Warehouse.countStoredItems();
        assert.strictEqual(evaluated, expected);
    });
    it('should count 0 for unknown items key', function () {
        const expected = 0;
        const evaluated = Warehouse.countStoredItem('foobar');
        assert.strictEqual(evaluated, expected);
    });
    it('should add an item in the warehouse', function() {
        const item = new Foo(bot);
        Warehouse.store(item);
    });
    it('should count all items in the warehouse', function() {
        assert.strictEqual(Warehouse.countStoredItems(), 1);
        Warehouse.store(new Foo(bot));
        Warehouse.store(new Foo(bot));
        assert.strictEqual(Warehouse.countStoredItems(), 3);
    });
    it('should return the storage content as string', function() {
        const expected = [`${new Foo(bot).constructor.name} : 3`];
        const evaluated = Warehouse.getAuditStorage();
        assert.deepStrictEqual(evaluated, expected);
    });
    it('should find an item in the storage', function() {
        const item = new Foo(bot);

        Warehouse.store(item);
        const found = Warehouse.findItem(item);
        const expected = found && found.uuid;
        const evaluated = item.uuid;

        assert.strictEqual(evaluated, expected);
    });
    it('should find an item index in the storage', function() {
        const item = new Foo(bot);
        let index;

        Warehouse.store(item);
        index = Warehouse.findItemIndex(item);

        const expected = 4;
        const evaluated = index;

        assert.strictEqual(expected, evaluated);
    });
    it('should remove an item from the storage', function() {
        const item = new Foo(bot);
        Warehouse.store(item);
        Warehouse.removeItem(item);

        const expected = undefined;
        const evaluated = Warehouse.findItem(item);

        assert.strictEqual(evaluated, expected);
    });
    it('should get the storage and do some checks', function() {
        const foo = new Foo(bot);

        const storage = Warehouse.getStorage();
        Warehouse.store(foo);

        assert.strictEqual(typeof storage, 'object');
        assert.strictEqual(Object.keys(storage).length, 1);
        assert.strictEqual(foo.constructor.name in storage, true);
        assert.strictEqual(Foo.name in storage, true);
    });
    it('should find item by type', function() {
        const type = 'Foo';
        const evaluated =  Warehouse.findItemByType(type);

        assert.strictEqual(evaluated.constructor.name, type);
    });
    it('should purge Foo from storage', function() {
        const type = 'Foo';
        const fooCountBeforePurge = Warehouse.countStoredItem(type);

        Warehouse.purgeByType(type);

        const fooCounter = Warehouse.countStoredItem(type);

        assert.strictEqual(fooCountBeforePurge > 0, true);
        assert.strictEqual(fooCounter, 0);
    });
    it('should purge Bar from storage', function() {
        Warehouse.store(new Bar(bot));
        const type = 'Bar';
        const barCountBeforePurge = Warehouse.countStoredItem(type);
        Warehouse.purgeByType(type);
        const barCounter = Warehouse.countStoredItem(type);

        assert.strictEqual(barCountBeforePurge > 0, true);
        assert.strictEqual(barCounter, 0);
    });
    it('should purge the storage', function() {
        Warehouse.purge();

        const expected = 0;
        const evaluated = Warehouse.countStoredItems();
        assert.strictEqual(evaluated, expected);
    });
    it('should need Bar', function() {
        Warehouse.purge();

        for (let i = 0; i < 30; i++) {
            Warehouse.store(new Foo(bot));
        }

        const expected = 'Bar';
        const evaluated = Warehouse.neededItem();
        assert.strictEqual(evaluated, expected);
    });
    it('should need Foo', function() {
        Warehouse.purge();

        for (let i = 0; i < 30; i++) {
            Warehouse.store(new Bar(bot));
        }

        const expected = 'Foo';
        const evaluated = Warehouse.neededItem();
        assert.strictEqual(evaluated, expected);
    });
    it('should need a Foobar', function() {
        for (let i = 0; i < 30; i++) {
            Warehouse.store(new Bar(bot));
            Warehouse.store(new Foo(bot));
        }

        const expected = 'Foobar';
        const evaluated = Warehouse.neededItem();
        assert.strictEqual(evaluated, expected);
        Warehouse.purge();
    });
    it('should return a empty wallet', function() {
        const expected = 0;
        const wallet = Warehouse.getWallet();

        assert.strictEqual(wallet, expected);
    });
    it('should sell an item', function () {
        const foo = new Foo(bot);
        Warehouse.store(foo);
        Warehouse.sellItem(foo);

        const wallet = Warehouse.getWallet();
        const expected = foo.value;

        assert.strictEqual(wallet, expected);
    });
    it('should create a new bot', function() {
        for (let i = 0; i <= 6; i++) {
            const foo = new Foo(bot);
            Warehouse.store(foo);
        }

        for (let i = 0; i <= 3; i++) {
            const foo = new Foo(bot);
            const bar = new Bar(bot);
            const foobar = new Foobar(foo, bar, bot)
            Warehouse.store(foobar);
            Warehouse.sellItem(foobar, false);
        }

        const expected = true;
        const evaluated = Warehouse.newBot(false);

        assert.strictEqual(evaluated, expected);
    })
});
