import { assert } from "chai";
import { Bot } from "../src/classes/Bot.class";



describe('[Bot]', function() {
    const bot = new Bot('Dummy');
    it('should create a bot', function() {
        assert.equal(bot.constructor.name, 'Bot');
    });
    it('should extract a Foo', function() {
        const foo = bot.extractFoo();
        const expected = 'Foo';
        const evaluated = foo.constructor.name;
        assert.equal(evaluated, expected);
    });
    it('should get the bot who extracted the Foo', () => {
        const foo = bot.extractFoo();
        const extractor = foo.bot;

        assert.equal(bot, extractor);
    });
});
