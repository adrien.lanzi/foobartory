import assert from "assert";
import { Bar } from "../src/classes/Bar.class";
import { Bot } from "../src/classes/Bot.class";

describe('[Bar]', function() {
    const bot = new Bot('Bar tester')
    it('bar value should be a number', function() {
        const bar = new Bar(bot);

        const expected = 'number';
        const evaluated = typeof bar.value;
        assert.strictEqual(expected, evaluated);
    });
    it('bar value should be 0.6', function() {
        const bar = new Bar(bot);

        const expected = 0.6;
        const evaluated = bar.value;
        assert.strictEqual(expected, evaluated);
    });
});