# Foobartory

---

## How to

### Run it :

$ nodemon

### Run tests & coverage :

$ npm run coverage

Open */coverage/Icov-report/index.html* to see the code coverage.

### Troubleshooting :

**nodemon** script blocked on Windows 10 PowerShell :

$ Set-ExecutionPolicy RemoteSigned -Scope CurrentUser

---

## Avant-propos du projet

Le but est de coder une chaîne de production automatique de `foobar`.

On dispose au départ de 2 robots, qui sont chacun capables d'effectuer plusieurs actions :

-   Se déplacer pour changer d'activité : occupe le robot pendant 5 secondes.
-   Miner du `foo` : occupe le robot pendant 1 seconde.
-   Miner du `bar` : occupe le robot pendant un temps aléatoire compris entre 0.5 et 2 secondes.
-   Assembler un `foobar` à partir d'un `foo` et d'un `bar` : occupe le robot pendant 2 secondes. L'opération a 60% de chances de succès ; en cas d'échec le `bar` peut être réutilisé, le `foo` est perdu.

Nous avons de grands entrepôts, la gestion des stocks n'est pas un problème.
En revanche, la législation impose la traçabilité des pièces ayant servi à fabriquer les `foobars` : chaque `foo` et chaque `bar` doivent avoir un numéro de série unique qu'on doit retrouver sur le `foobar` en sortie d'usine

On souhaite ensuite accélérer la production pour prendre rapidement le contrôle du marché des `foobar`. Les robots peuvent effectuer de nouvelles actions:

-   Vendre des `foobar` : 10s pour vendre de 1 à 5 foobar, on gagne 1€ par foobar vendu
-   Acheter un nouveau robot pour 3€ et 6 `foo`, 0s
