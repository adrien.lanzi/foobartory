interface Extractable {
    extractionDifficulty: number;

    extract(): boolean;
}