import { v4 as uuid4 } from 'uuid';
import { Bot } from './Bot.class';

export abstract class Item {
    private readonly _uuid: string;
    private readonly _bot: Bot;
    abstract readonly _value: number;

    constructor(bot: Bot) {
        this._uuid = uuid4();
        this._bot = bot;
    }

    public get uuid(): string {
        return this._uuid;
    }

    public get bot(): Bot {
        return this._bot;
    }

    public abstract get value(): number;
}
