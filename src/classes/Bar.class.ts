import { Item } from "./Item.class";
import { Bot } from "./Bot.class";

export class Bar extends Item {
    public readonly _value: number = 0.6;
    
    constructor(bot: Bot) {
        super(bot);
    }
    
    public get value(): number {
        return this._value;
    }

    static async extract(bot: Bot): Promise<Bar> {
        return await new Promise((resolve) => setTimeout(() => resolve(new Bar(bot)), 1000));
    }
}
