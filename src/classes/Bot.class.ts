import { v4 as uuid4 } from 'uuid';
import {Warehouse} from "./warehouse.namespace";
import {Foo} from "./Foo.class";
import chalk from "chalk";
import {Bar} from "./Bar.class";
import {Foobar} from "./Foobar.class";
import removeItem = Warehouse.removeItem;
import store = Warehouse.store;
import findItemByType = Warehouse.findItemByType;
import getWallet = Warehouse.getWallet;

const startOfScript = Date.now();

export class Bot {
    private _uuid: string;
    private _name: string = '';
    private currentTask: any = '';
    private taskHistory: string[] = [];

    constructor(name?: string|null) {
        this._uuid = uuid4();
        this.name = name || this._uuid;

        this.speak('I am alive !');
    }

    gotoWork() {
        this.speak('I go to work !');
        this.todo();
    }

    todo(): void {
        const neededItem = Warehouse.neededItem();
        const foobarCount = Warehouse.countStoredItem(Foobar.name);
        let task = 'idle';

        if (foobarCount > 1) {
            this.sell(Foobar.name);
            return;
        }

        switch (neededItem) {
            case 'Foo':
                task = `mine::Foo`;
                if (this.currentTask === task || this.currentTask === 'idle') {
                    this.currentTask = `mine::Foo`;
                    this.mineFoo();
                } else {
                    this.currentTask = 'idle';
                    this.move();
                }
                break;
            case 'Bar':
                task = `mine::Bar`;
                if (this.currentTask === task || this.currentTask === 'idle') {
                    this.currentTask = `mine::Bar`;
                    this.mineBar();
                } else {
                    this.currentTask = 'idle';
                    this.move();
                }
                break;
            case 'Foobar':
                task = 'build::Foobar';
                if (this.currentTask === task || this.currentTask === 'idle') {
                    this.currentTask = 'build:Foobar';
                    const foo = findItemByType('Foo');
                    const bar = findItemByType('Bar');
                    this.assembleFoobar(foo, bar, this);
                } else {
                    this.currentTask = 'idle';
                    this.move();
                }
                break;
            default:
                break;
        }
    }

    move() {
        const duration = 5000;
        this.idle(() => this.speak('I moved !'), duration);
    }

    async extractFoo() {
        return await Foo.extract(this);
    }

    async extractBar() {
        return await Bar.extract(this);
    }

    async mineFoo() {
        const foo = await this.extractFoo();
        Warehouse.store(foo);
        this.speak(`I mined a Foo !`);
        this.todo();
    }

    async mineBar() {
        const bar = await this.extractBar();
        Warehouse.store(bar);
        this.speak(`I mined a Bar !`);
        this.todo();
    }

    async assembleFoobar(foo: Foo, bar: Bar, bot: Bot) {
        const duration = 1000;
        removeItem(foo);
        removeItem(bar);

        const foobar = await Foobar.build(foo, bar, this);

        if (foobar === false) {
            Warehouse.store(bar);
            this.speak('I failed to build the Foobar.');
        } else {
            Warehouse.store(foobar);
            this.speak('I built a Foobar !');
        }

        this.todo();
    }

    sell(itemType: string): void {
        const taskDuration = 10000;
        const toSell = Math.floor(Math.random() * 5) + 1;
        let sold = 0;


        for (let i = toSell; i > 0; i--) {
            const item = Warehouse.findItemByType(itemType);
            if (item) {
                Warehouse.sellItem(item);
                sold++;
            } else {
                break;
            }
        }

        this.idle(() => {
            this.speak(`I sold ${sold} Foobar on ${toSell}, wallet is now ${chalk.green(getWallet())}$.`);
        }, taskDuration);
    }

    speak(sentence: string) {
        console.log(`[${chalk.gray(Date.now() - startOfScript)} ${chalk.red(`BOT ${this.name}`)}] : ${sentence} `);
    }

    async idle(task: Function, delay: number): Promise<void> {
        return await new Promise(() => setTimeout(() => {
            task();
            this.todo()
        }, delay));
    }

    get name() {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }
}
