import { Item } from "./Item.class";
import {Foo} from "./Foo.class";
import {Bar} from "./Bar.class";
import { Foobar } from "./Foobar.class";
import {Bot} from "./Bot.class";

export interface storageInterface {
    [key: string]: Item[]
}

export namespace Warehouse {
    const storage: storageInterface = {};
    let wallet: number = 0;

    export function getStorage() {
        return storage;
    }

    export function sellItem(item: Item, buyNewBot = true): void {
        wallet += item.value;
        removeItem(item);
        if (buyNewBot) {
            newBot();
        }
    }

    export function newBot(work = true): boolean {
        if (wallet >= 3 && countStoredItem('Foo') >= 6) {
            wallet -= 3;

            for (let i = 0; i < 6; i++) {
                let foo = findItemByType('Foo');
                removeItem(foo);
            }

            const newBot = new Bot()
            if (work) {
                newBot.gotoWork();
            }
            return true;
        } else {
            return false;
        }
    }

    export function getWallet(): number {
        return wallet;
    }

    export function countStoredItem(key: string) {
        return key in storage && Array.isArray(storage[key]) ? storage[key].length : 0;
    }

    export function countStoredItems() {
        return Object.keys(storage).reduce((sum, value) => sum + countStoredItem(value), 0);
    }

    export function findItemIndex(item: Item) {
        const key = item.constructor.name;
        return storage[key].findIndex(storedItem => storedItem.uuid === item.uuid)
    }

    export function findItem(item: Item) {
        const key = item.constructor.name;
        return storage[key].find(storedItem => storedItem.uuid === item.uuid);
    }

    export function findItemByType(type: string) {
        return storage[type][0];
    }

    export function removeItem(item: Item) {
        const key = item.constructor.name;
        const index = findItemIndex(item);

        storage[key].splice(index, 1);
    }

    export function neededItem() {
        const fooCount = countStoredItem(Foo.name);
        const barCount = countStoredItem(Bar.name);
        const foobarCount = countStoredItem(Foobar.name);

        if (fooCount <= 20) {
            return Foo.name;
        }

        if (fooCount >= 20 && barCount > 20) {
            return Foobar.name;
        }

        return Bar.name;
    }

    export function getAuditStorage() {
        return Object.keys(storage).map(itemName => `${itemName} : ${storage[itemName].length}`);
    }

    export function auditStorage() {
        console.log(getAuditStorage());
    }

    export function store(item: Item) {
        const key = item.constructor.name;
        if (!(key in storage)) {
            storage[key] = [];
        }
        storage[key].push(item);
    }

    export function purgeByType(type: string) {
        return storage[type] = [];
    }

    export function purge() {
        return Object.keys(storage).forEach(type => delete storage[type]);
    }
}
