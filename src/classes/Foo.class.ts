import { Item } from "./Item.class";
import { Bot } from "./Bot.class";

export class Foo extends Item {
    public readonly _value: number = 0.5;
    constructor(bot: Bot) {
        super(bot);
    }
    
    public get value(): number {
        return this._value;
    }

    static async extract(bot: Bot): Promise<Foo> {
        return await new Promise((resolve) => setTimeout(() => resolve(new Foo(bot)), 500));
    }
}
