import {Foo} from "./Foo.class";
import {Bar} from "./Bar.class";
import {Item} from "./Item.class";
import {Bot} from "./Bot.class";
import { Warehouse } from "./warehouse.namespace";

export class Foobar extends Item {
    private readonly _foo: Foo;
    private readonly _bar: Bar
    public readonly _value: number = 1;

    constructor(foo: Foo, bar: Bar, bot: Bot) {
        super(bot);
        this._foo = foo;
        this._bar = bar;
    }

    public get foo(): Foo {
        return this._foo;
    }

    public get bar(): Bar {
        return this._bar;
    }

    public get value(): number {
        return this._value;
    }

    static async build(foo: Foo, bar: Bar, bot: Bot): Promise<Foobar|false> {
        return await new Promise((resolve) => {
            setTimeout(() => {
                const isWasted = Math.random() < 0.6;
                resolve(isWasted ? false : new Foobar(foo, bar, bot));
            }, 1000);
        });
    }
}
